import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    Widget titleSection = Container(
      padding: const EdgeInsets.all(20), //creates padding around the outside
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Eiffel Tower', style: TextStyle (
                    fontWeight: FontWeight.bold,
                  ),
                  ),
                ),
                Text('Paris, France', style: TextStyle(
                  color: Colors.pinkAccent[200],),
                ),
              ],
            ),
          ),
          DownloadWidget(),
          FavoriteWidget(),
        ],
      ),
    ); //Title Section

    Widget titleSection2 = Container(
      padding: const EdgeInsets.all(20), //creates padding around the outside
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Palace of Versailles', style: TextStyle (
                    fontWeight: FontWeight.bold,
                  ),
                  ),
                ),
                Text('Versailles, France', style: TextStyle(
                  color: Colors.pinkAccent[200],),
                ),
              ],
            ),
          ),
          DownloadWidget(),
          FavoriteWidget(),
        ],
      ),
    ); //Title Section2


    Widget buttonSection = Container(
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.pink, Icons.arrow_back, 'Back'),
          _buildButtonColumn(Colors.black54, Icons.home, 'Home'),
          _buildButtonColumn(Colors.pink, Icons.arrow_forward, 'Next'),
        ],
      ),
    );//Button Section

    Widget textSection = Container(
      padding: const EdgeInsets.all(20),
          child: Text(
        'The summer of 2018, my family decided to go to take a trip around Europe.'
        'We visited Ireland, England, France, and Italy. We each had a favorite place '
        'and mine was Paris, France. I have been wanting to go to France since I was  '
        'little. It was always a dream of mine and I was the designated translator since '
        'I took three years of French. We spent three days there and it was amazing. '
        'My favorite spots that we visited was the Eiffel Tower and the Palace of Versailles.',
        softWrap: true,
    ),
    );//Text Section

    Widget textSection2 = Container(
      padding: const EdgeInsets.all(20),
      child: Text(
        'The Palace of Versailles, also known as Versailles was really fancy. '
        'It was originally a hunting lodge and King Louis XIV decided to '
        'move the entire court and governmet there so he could have more '
        'control. It was absolutely beautiful, one of my favorite movies '
        'is Marie Antoinette which is about her life at Versailles. It is '
        'one thing to see Versailles in the movies and to see it in real life.',
        softWrap: true,
      ),
    );//Text Section2

    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.pinkAccent,
          title: Text('Layout Homework'),
        ),
        body: ListView(
          children: [
            buttonSection,
            Image.asset(
              'images/eiffel.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            titleSection,
            textSection,
            Image.asset(
              'images/versailles.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            titleSection2,
            textSection2,
          ]
        ),
        ),
      );



  } //BUILD End Bracket

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            )
          )
        ),
      ],
    );
  } //Button Struct

} //MY APP

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavored = false;
  int _favoriteCount = 41;
  @override
  Widget build(BuildContext context){
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            padding: EdgeInsets.all(0),
            alignment: Alignment.centerRight,
            icon: (_isFavored ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
            color: Colors.pink,
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$_favoriteCount'),
          )
        ),
        Container(
         padding: EdgeInsets.all(0),
            child: _isFavored ? Text('Image Liked', style: TextStyle(color: Colors.pink),) : null,
        ),
    ],
    );
  }
  void _toggleFavorite() {
    setState(() {
      if(_isFavored) {
        _favoriteCount -= 1;
        _isFavored = false;
      }
      else {
        _favoriteCount += 1;
        _isFavored = true;
      }
    });
  }
}

class DownloadWidget extends StatefulWidget {
  @override
  _DownloadWidgetState createState() => _DownloadWidgetState();
}

class _DownloadWidgetState extends State<DownloadWidget> {
  bool _isDownloaded = false;
  @override
  Widget build(BuildContext context){
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            padding: EdgeInsets.all(0),
            alignment: Alignment.centerRight,
            icon: (_isDownloaded? Icon(Icons.arrow_downward, color: Colors.pink, ) : Icon(Icons.arrow_downward, color: Colors.black38)),
            onPressed: _toggleFavorite,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: _isDownloaded ? Text('Image Downloaded', style: TextStyle(color: Colors.pink),) : null,
        ),
      ],
    );
  }
  void _toggleFavorite() {
    setState(() {
      if(_isDownloaded) {
        _isDownloaded = false;
      }
      else {
        _isDownloaded = true;
      }
    });
  }
}